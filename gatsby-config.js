module.exports = {
  siteMetadata: {
    title: `Tyler Harnadek`,
    description: `Personal website for Tyler Harnadek`,
    author: `Tyler Harnadek`,
    tagline: `University of Victoria`,
    github: `tharnadek`,
    gitlab: `tharnadek`,
    linkedin: `tyler-harnadek-93201b10b`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `content`,
        path: `${__dirname}/src/content/`,
      },
    },
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        extensions: [`.md`, `.mdx`],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-smoothscroll`,
    {
      resolve: `gatsby-plugin-netlify`,
      options: {
        headers: {
          "/*": [

            "Access-Control-Allow-Origin: null",
            // reference: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Feature-Policy
            // TODO: remove feature-policy once permission-policy is more widely accepted
            "Feature-Policy: autoplay: 'none'; camera: 'none'; document-domain: 'none'; encrypted-media: 'none'; fullscreen: 'self'; geolocation: 'none'; microphone: 'none'; midi: 'none'; payment: 'none'; vr: 'none'",
            "Referrer-Policy: origin",
            // TODO: Add hsts once I'm finished with https support
            // "Strict-Transport-Security: max-age=31536000; includeSubDomains; preload",
            "Upgrade-Insecure-Requests: 1",
            "X-Content-Type-Options: nosniff",
            "X-Frame-Options: deny",
            "X-XSS-Protection: 1; mode=block",
          ],
        },
        allPageHeaders: [], // option to add headers for all pages. `Link` headers are transformed by the below criteria
        mergeSecurityHeaders: true, // boolean to turn off the default security headers
        mergeLinkHeaders: true, // boolean to turn off the default gatsby js headers
        mergeCachingHeaders: true, // boolean to turn off the default caching headers
        generateMatchPathRewrites: true, // boolean to turn off automatic creation of redirect rules for client only paths
      },
    },
  ],
}
