import React from "react"
import PropTypes from "prop-types"
import { Menu as Icon } from "react-feather"

const Burger = ({ open, setOpen }) => {
  open = false
  return (
    <button className="logo grow" onClick={() => setOpen(!open)}>
      <Icon />
    </button>
  )
}

Burger.propTypes = {
  open: PropTypes.bool.isRequired,
  setOpen: PropTypes.func.isRequired,
}

const Menu = ({ open, setOpen }) => {
  if (open) return <p>Menu!!</p>
}

Menu.propTypes = {
  open: PropTypes.bool.isRequired,
  setOpen: PropTypes.func.isRequired,
}

export { Burger, Menu }
