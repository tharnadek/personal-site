*Personal Website built with Gatsby*

[![Netlify Status](https://api.netlify.com/api/v1/badges/bd389c1b-949a-41aa-9ba3-c68cdc3fddc9/deploy-status)](https://app.netlify.com/sites/tharnadek-portfolio/deploys)


1.  **Start developing.**

    Navigate into your new site’s directory and start it up.

    ```sh
    gatsby develop
    ```

1.  **Open the source code and start editing!**

    Your site is now running at `http://localhost:8000`!

